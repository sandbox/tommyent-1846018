<?php


/**
 * @file
 * sendy module admin settings.
 */

/**
 * Return the sendy settings form.
 */
function sendy_admin_settings() {
  $form['sendy_installation_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Your Sendy Installation URL'),
      '#required' => TRUE,
      '#default_value' => variable_get('sendy_installation_url', ''),
      "#description" => t('The URL of your Sendy installation.')
  );

  $form['sendy_list_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Your Sendy List  ID'),
      '#required' => TRUE,
      '#default_value' => variable_get('sendy_list_id', ''),
      "#description" => t('The list id you want to subscribe a user to. This unique id can be found under view all lists section named ID.')
  );

    $form['sendy_user_register']  = array(
    '#type' => 'checkbox',
    '#title' => t('Show subscription on the user registration form'),
    '#default_value' => variable_get('sendy_user_register', TRUE),
    '#description' => t('If set, users can subscribe when registering for the site'),
  );

   return system_settings_form($form);
}
